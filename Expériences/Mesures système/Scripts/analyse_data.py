import csv
import math
import locale
import datetime
import collections
import numpy as np
import scipy.stats as stats
import matplotlib.pyplot as plt

DOOL_CSV = "dool.csv"
UM25C_CSV = "UM25C.csv"
TEST_CSV = "test.csv"
NTP_OFFSET = "-0.002857"

Test = collections.namedtuple(
        "Test",
        ("start", "end", "url"),
)

Dool = collections.namedtuple(
    "Dool",
    ("date","cpu_usr","cpu_sys","cpu_idle","cpu_wait","cpu_stl","disk_read","disk_write", "mem_used", "mem_free", "mem_buff", "mem_cach", "net_recv","net_send", "net_p_recv", "net_p_send"),
)

UM25C = collections.namedtuple(
    "UM25C",
    ("date","tension","intensite","puissance","resistance"),
)

def read_test_csv(filename, ntpOffset):
    locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')
    info = []
    with open(filename) as f:
        for row in csv.DictReader(f):
            try:
                start = datetime.datetime.fromtimestamp(
                    float(row["test_start"])+float(ntpOffset)
                )
                end = datetime.datetime.fromtimestamp(
                    float(row["test_end"])+float(ntpOffset)
                )
                url = row["url"]
                info.append(Test(start, end, url))
            except:
                print("Ligne invalide :")
                print(row)
                pass
    return info

def read_dool_csv(filename, ntpOffset):
    locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')
    measures = []
    with open(filename) as f:
        for row in csv.DictReader(f):
            try:
                cpu_usr = float(row["usr"])
                cpu_sys = float(row["sys"])
                cpu_idle = float(row["idl"])
                cpu_wait = float(row["wai"])
                cpu_stl = float(row["stl"])
                disk_read = float(row["read"])
                disk_write = float(row["writ"])
                mem_used = float(row["used"])
                mem_free = float(row["free"])
                mem_buff = float(row["buff"])
                mem_cach = float(row["cach"])
                net_recv = float(row["recv"])
                net_send = float(row["send"])
                net_p_send = float(row["#send"])
                net_p_recv = float(row["#recv"])
                date = datetime.datetime.strptime(
                    str(datetime.datetime.now().year) + "-" + row["time"], "%Y-%b-%d %H:%M:%S"
                ) + datetime.timedelta(seconds=ntpOffset)
                measures.append(Dool(date, cpu_usr, cpu_sys, cpu_idle, cpu_wait, cpu_stl, disk_read, disk_write, mem_used, mem_free, mem_buff, mem_cach, net_recv, net_send, net_p_recv, net_p_send))
            except Exception as e:
                print("Exception :"+str(e))
                print("Ligne invalide :")
                print(row)
                pass
    return measures

def read_um25c_csv(filename):
    locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')
    measures = []
    with open(filename) as f:
        for row in csv.DictReader(f):
            try:
                tension = float(row["tension"])
                intensite = float(row["intensite"])
                puissance = float(row["puissance"])
                resistance = float(row["resistance"])
                date = datetime.datetime.strptime(
                    row["date"] + " " + row["heure"], "%Y-%m-%d %H:%M:%S.%f"
                )
                measures.append(UM25C(date, tension, intensite, puissance, resistance))
            except:
                print("Ligne invalide :"+row)
                pass
    return measures

def get_plot(metricsDool, metricsUM25C, name, ntpOffset=float(NTP_OFFSET)):
    mDool = read_dool_csv(DOOL_CSV, ntpOffset)
    mUM25C = read_um25c_csv(UM25C_CSV)
    for metric in metricsDool:
        plt.plot([m.date for m in mDool],[getattr(m,metric) for m in mDool], label=metric)
    for metric in metricsUM25C:
        plt.plot([m.date for m in mUM25C],[getattr(m,metric) for m in mUM25C], label=metric)
    plt.savefig(name)

# def main():
#     um25c = read_um25c_csv(UM25C_CSV)
#     dool = read_dool_csv(DOOL_CSV)
