#! /usr/bin/python3

###################################################################################
#            Script d'analyse des données du multimètre et de Dool                #
#                                                                                 #
#   TODO : éventuellement ajouter des graphes pour certaines mesures système      #
###################################################################################

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from datetime import datetime, timedelta
import csv
from urllib.parse import urlparse

# Chemins vers les fichiers csv des expériences à traiter
EXPERIENCE_03='resultats/web_03/'
EXPERIENCE_04='resultats/web_04/'
CSV_MULTIMETRE='UM25C_2.csv'
CSV_DOOL='dool.csv'
CSV_TEST='test.csv'

METRIQUES_DOOL = [('CPU','usr'),('RAM','used'),('Lectures disque','read'),('Écritures disque','writ'),('BUFFER','buff'),('CACHE','cach'),('Données réseau reçues','recv'),('Données réseau envoyées','send')]

# En-tête du fichier csv pour les données de Dool
DOOL_COLUMNS=["usr","sys","idl","wai","stl","read","writ","used","free","buff","cach","recv","send","time","#recv","#send"]

# En-tête du fichier csv pour les données du multimètre
MULTIMETRE_COLUMNS=["date", "tension", "intensite", "puissance", "resistance"]

# En-tête du fichier résumant les requêtes réalisées avec leur temps d'exécution
TEST_COLUMNS=["start", "end", "url"]

# Construction d'un objet datetime à partir de la date des csv du multimètre
str2date_multimetre = lambda x: datetime.strptime(x.decode("utf-8"), '%Y-%m-%d %H:%M:%S.%f')
# Idem pour dool, on ajoute l'année pour pouvoir comparer les dates avec celles obtenues dans le csv du multimètre
str2date_dool = lambda x: datetime.strptime(x.decode("utf-8"), '%b-%d %H:%M:%S').replace(year=2021)

# Retourne une liste des listes : groupe les experiences par URL a partir du nombre de requetes
# Exemple : pour une experience avec 8 urls et 20 requetes, retourne 8 liste de 20 elements datetime
def getIntervals(EXPERIENCE,n):
    f=open(EXPERIENCE+CSV_TEST,'r')
    csv_reader = csv.reader(f, delimiter=',')
    line=0
    full_exp=[]
    urls=[]
    for row in csv_reader:
        if(line>0):
            # Recaler le temps entre les fichiers CSV (différence de 1h 30s)
            full_exp.append(datetime.utcfromtimestamp(float(row[1]))+ timedelta(hours=1) + timedelta(seconds=-30))
            if str(row[2]) not in urls:
                urls.append(str(row[2]))
        else:
            line+=1

    intervals = [full_exp[i * n:(i + 1) * n] for i in range((len(full_exp) + n - 1) // n )]
    return (intervals,urls)

# Retourne un graphe de la consommation pour chaque URL
def plotMultimetre(EXPERIENCE,n):
    # Récupération des données à partir du fichier  CSV
    data = np.genfromtxt(EXPERIENCE+CSV_MULTIMETRE,dtype=None, delimiter=",", skip_header=1, names=MULTIMETRE_COLUMNS,converters = {0: str2date_multimetre})
    # Liste des temps de requête pour chaque url
    intervals = getIntervals(EXPERIENCE,n)[0]
    # Liste unique des urls requêtées
    urls = getIntervals(EXPERIENCE,n)[1]
    # Pour chaque url, on va chercher la date de la dernière requête à afficher
    # Liste des index de fin pour chaque url dans le jeu de données
    fins=[]

    for i in range(len(intervals)):
        end=intervals[i][-1]
        fins.append(np.where(data['date']<=end)[0][-1])

    # On cherche l'index dans nos données qui correspond à cette date
    # On tronque les données pour avoir celles inférieures ou égales à la date de fin
    truncated_data=[]
    for i in range(len(fins)):
        if i==0:
            truncated_data.append(data[:fins[i]])
        else:
            truncated_data.append(data[fins[i-1]:fins[i]])

    print('Expérience : ' + EXPERIENCE)

    # Pour chaque URL, on construit un graphe
    for i in range(len(truncated_data)):
        url = urlparse(urls[i])
        site=url.netloc

        print('+ [Multimètre] Traitement du site ' + str(site))

        plt.figure(num=None, figsize=(20, 6), dpi=200, facecolor='w', edgecolor='k')
        # Affichage des dates en abscisses
        plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%H:%M:%S'))
        # On affiche les abscisses toutes les minutes
        plt.gca().xaxis.set_major_locator(mdates.MinuteLocator(interval=1))

        # Légendes et titre
        plt.xlabel('temps')
        plt.ylabel('puissance (w)')
        plt.title('Graphique de puissance pour ' + str(n) + ' requêtes pour l\'url : \n ' + str(site))

        # Affichage
        plt.plot(truncated_data[i]['date'], truncated_data[i]['puissance'])
        plt.gcf().autofmt_xdate()

        plt.savefig(EXPERIENCE+'graphiques_multimetre/'+ str(site) +'.png',format='png')
        plt.close()

# Retourne un graphe de métriques Dool pour chaque URL
# Pou l'instant, on ne traite que les métriques du CPU : ce sont les plus significatives
def plotDool(EXPERIENCE,n):
    # Fonction similaire à la fonction plotMultimètre, adaptée aux dataset de Dool
    data = np.genfromtxt(EXPERIENCE+CSV_DOOL,dtype=None, delimiter=",", skip_header=1, names=DOOL_COLUMNS,converters = {'time': str2date_dool})
    intervals = getIntervals(EXPERIENCE,n)[0]
    urls = getIntervals(EXPERIENCE,n)[1]

    # Pour chaque url, on va chercher la date de la dernière requête à afficher
    # Liste des index de fin pour chaque url dans le jeu de données
    fins=[]

    for i in range(len(intervals)):
        end=intervals[i][-1]
        fins.append(np.where(data['time']<=end)[0][-1])

    # On cherche l'index dans nos données qui correspond à cette date
    # On tronque les données pour avoir celles inférieures ou égales à la date de fin
    truncated_data=[]
    for i in range(len(fins)):
        if i==0:
            truncated_data.append(data[:fins[i]])
        else:
            truncated_data.append(data[fins[i-1]:fins[i]])

    print('Expérience : ' + EXPERIENCE)        

    for i in range(len(truncated_data)):
        url = urlparse(urls[i])
        site=url.netloc

        print('+ [Dool] Traitement du site ' + str(site))

        for metrique in METRIQUES_DOOL:

            print(' + + Graphe pour la métrique : ' + metrique[1]) 

            plt.figure(num=None, figsize=(20, 6), dpi=200, facecolor='w', edgecolor='k')
            # Affichage des dates en abscisses
            plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%H:%M:%S'))
            # On affiche les abscisses toutes les minutes
            plt.gca().xaxis.set_major_locator(mdates.MinuteLocator(interval=1))

            # Légendes et titre
            plt.xlabel('temps')
            plt.ylabel(metrique[0])
            plt.title('Graphique ' + metrique[0] + ' pour ' + str(n) + ' requêtes pour le site : \n ' + str(site))

            # Affichage
            plt.plot(truncated_data[i]['time'], truncated_data[i][metrique[1]])
            plt.gcf().autofmt_xdate()

            plt.savefig(EXPERIENCE+'graphiques_dool/' + metrique[1] + '/' + str(site) + '_' + metrique[1] +'.png',format='png')
            plt.close()

def main():
    # Construction des graphiques pour les deux jeux de données, sur le multimètre et sur Dool
    plotMultimetre(EXPERIENCE_03,20)
    plotMultimetre(EXPERIENCE_04,20)
    plotDool(EXPERIENCE_03,20)
    plotDool(EXPERIENCE_04,20)

if __name__ == "__main__":
    main()
