#!/bin/bash

#-- Paramètres ----------------------------------------------------------------#
# Paramètres pour configurer l'accès à la Raspberry Pi
SSH_HOST_IP="192.168.48.38"
SSH_HOST_USER="pi"
SSH_RSA_KEY="~/.ssh/dev_rsa"

# Paramètres pour configurer l'accès au multimètre et à l'outil rdumtool
UM25C_MAC="00:15:A3:00:4C:49"
RDUMTOOL_HOME=/mnt/DATA/thomas/Documents/utc/enseignement/gi05/PR/only_local/rdumtool-master

# Paramètres pour configurer l'utilisation de l'outil de monitoring dool
# Attention : le répertoire suivant est sur la Raspberry Pi
DOOL_HOME=/home/pi/dool
DOOL_OPTS="-cdmnt --net-packets"

# Paramètre optionnel pour configurer le temps d'attente entre chaque exécution
# du test. Valeur en secondes
TIMEOUT=""

# Chemin d'accès au script de test et nombre d'exécutions
# Attention : le fichier suivant est sur la Raspberry Pi. S'assurer que le
# shebang existe et que le fichier possède les droits d'exécution.
TEST_SCRIPT_PATH=/home/pi/script.py
# Nombre d'exécutions du test
TEST_EXECUTION_NB=20
# Variable spécifique au test web-URL. Liste les URL à requêter.
TEST_URLS=('https://www.lefigaro.fr/flash-actu/deces-de-vge-macron-decrete-un-jour-de-deuil-national-le-9-decembre-20201203'
'https://www.lemonde.fr/politique/article/2020/12/03/mort-de-valery-giscard-d-estaing-emmanuel-macron-decrete-un-jour-de-deuil-national_6062099_823448.html'
'https://www.francetvinfo.fr/politique/mort-de-valery-giscard-d-estaing/mort-de-valery-giscard-d-estaing-emmanuel-macron-decrete-un-jour-de-deuil-national-le-mercredi-9-decembre_4205837.html'
'https://www.courrier-picard.fr/id145444/article/2020-12-03/giscard-destaing-macron-decrete-un-jour-de-deuil-national-mercredi-9-decembre'
'https://www.sudouest.fr/2020/12/03/mort-de-giscard-d-estaing-macron-decrete-un-jour-de-deuil-national-le-9-decembre-8148657-10407.php'
'https://www.france24.com/fr/france/20201209-en-france-une-journ%C3%A9e-de-deuil-national-en-hommage-%C3%A0-val%C3%A9ry-giscard-d-estaing'
'https://www.lepoint.fr/politique/mort-de-valery-giscard-d-estaing-macron-decrete-un-jour-de-deuil-national-le-9-decembre-03-12-2020-2404133_20.php'
'http://95.142.172.54/PRPierre/test.html')
# Navigateur web utilisé ('firefox' ou 'chromium')
TEST_WEBBROWSER="-b firefox"
# Verbosité du test ('-v'), par défaut activée si cette option est requise
# ici-même
TEST_VERBOSITY=""


#-- Options -------------------------------------------------------------------#

# Option VERBOSE -v pour des affichages sur stderr pendant l'exécution
VERBOSE=0
if [ "x$1" = "x-v" ]; then
		VERBOSE=1
		TEST_VERBOSITY="-v"
fi

# Option HELP -h
if [ "x$1" = "x-h" ]; then
		echo "+ $0 : récupération de mesures lors de la réalisation d'un test"
		echo "  usage : $0 script_test | tee fichier_resultat (éditer $0 pour les paramètres)"
		echo "          option -v : affichages pendant l'exécution"
		echo "          option -h : affichage de cette aide"
		exit
fi

# Cltr C => interruption du test
terminate() {
        if [ -z "$DOOL_PID" ]; then
            ssh -i ${SSH_RSA_KEY} ${SSH_HOST_USER}@${SSH_HOST_IP} "kill ${DOOL_PID}"
			1>&2 echo "+ Processus dool (PID: ${DOOL_PID}) tué"
        fi
        if [ -z "$RDUMTOOL_PID" ]; then
            kill ${RDUMTOOL_PID}
			1>&2 echo "+ Processus rdumtool (PID: ${RDUMTOOL_PID}) tué"
        fi
		1>&2 echo "+ fin"
		exit
}
trap "terminate" INT

#-- Corps du programme --------------------------------------------------------#
# NB : sur stderr pour ne pas polluer le fichier résultat avec $0 > res
1>&2 echo "$0 : récupération de mesures lors de la réalisation d'un test"
1>&2 echo "+ PID=$BASHPID"

# Vérification des droits d'exécution du script de test
if [ `ssh -i ${SSH_RSA_KEY} ${SSH_HOST_USER}@${SSH_HOST_IP} "[[ -x ${TEST_SCRIPT_PATH} ]]"; echo $?` != 0 ]; then
        echo "- condition: $TEST_SCRIPT_PATH doit être exécutable"
        exit
else
		if [ $VERBOSE -eq 1 ] ; then
        		1>&2 echo "+ ok: $TEST_SCRIPT_PATH est exécutable"
		fi
fi

# Suppression préventive (dool écrit à la suite du fichier si il existe déjà)
if [ $VERBOSE -eq 1 ] ; then
        1>&2 echo "+ Suppression du potentiel ancien fichier dool.csv sur RPI"
fi
ssh -i ${SSH_RSA_KEY} ${SSH_HOST_USER}@${SSH_HOST_IP} 'rm -f dool.csv'


# Synchronisation du temps
echo "+ uptime RPI"
ssh -i ${SSH_RSA_KEY} ${SSH_HOST_USER}@${SSH_HOST_IP} 'uptime'
echo "+ NTP offset"
ntpdate -q $SSH_HOST_IP

if [ $VERBOSE -eq 1 ] ; then
        1>&2 echo "+ Lancement de dool sur RPI"
fi
DOOL_PID=$( ssh -i ${SSH_RSA_KEY} ${SSH_HOST_USER}@${SSH_HOST_IP} "nohup ${DOOL_HOME}/dool ${DOOL_OPTS} --output dool.csv >/dev/null & echo \$!" )

if [ $VERBOSE -eq 1 ] ; then
        1>&2 echo "+ Rédaction de l'en-tête CSV du fichier UM25C.csv"
fi
cat <<EOF > /tmp/UM25C.csv
"date","heure","tension","intensite","puissance","resistance"
EOF

if [ $VERBOSE -eq 1 ] ; then
        1>&2 echo "+ Suppression du potentiel ancien fichier UM25C.txt"
fi
rm -f /tmp/UM25C.txt

if [ $VERBOSE -eq 1 ] ; then
        1>&2 echo "+ Lancement de rdumtool"
fi
${RDUMTOOL_HOME}/rdumtool --device-type=UM25C --bluetooth-device=${UM25C_MAC} --watch 1 2> /dev/null | grep -e "^USB" -e "^Collection time" >> /tmp/UM25C.txt &
RDUMTOOL_PID=`ps aux | grep rdumtool | head -1 | awk '{print $2}'`

if [ $VERBOSE -eq 1 ] ; then
		1>&2 echo "+ Rédaction de l'en-tête CSV du fichier test.csv"
fi
cat <<EOF > /tmp/test.csv
"test_start","test_end","url"
EOF
scp -i ${SSH_RSA_KEY} /tmp/test.csv ${SSH_HOST_USER}@${SSH_HOST_IP}:/tmp/test.csv
rm -f /tmp/test.csv

if [ $VERBOSE -eq 1 ] ; then
        1>&2 echo "+ Début du test"
        1>&2 echo "++ Date : "`date`
		1>&2 echo "*** Début des messages du script de test ***"
fi

# Appel au script de test
for URL in ${TEST_URLS[*]}; do
	ssh -i ${SSH_RSA_KEY} ${SSH_HOST_USER}@${SSH_HOST_IP} "${TEST_SCRIPT_PATH} ${TEST_VERBOSITY} ${TEST_WEBBROWSER} ${URL} ${TEST_EXECUTION_NB} ${TIMEOUT}"
done

if [ $VERBOSE -eq 1 ] ; then
		1>&2 echo "*** Fin des messages du script de test ***"
        1>&2 echo "+ Fin du test"
        1>&2 echo "++ Date : "`date`
fi

if [ $VERBOSE -eq 1 ] ; then
        1>&2 echo "+ Arrêt de rdumtool"
fi
kill -INT ${RDUMTOOL_PID}

if [ $VERBOSE -eq 1 ] ; then
        1>&2 echo "+ Arrêt de dool"
fi
ssh -i ${SSH_RSA_KEY} ${SSH_HOST_USER}@${SSH_HOST_IP} "kill -INT ${DOOL_PID}"

if [ $VERBOSE -eq 1 ] ; then
	1>&2 echo "+ Vérification du contenu du fichier /tmp/UM25C.txt"
fi
DOOL_LINES=`wc -l /tmp/UM25C.txt | cut -d' ' -f1`
while [[ $DOOL_LINES -eq 0 ]]; do
	1>&2 echo "+ Sleeping 5 more seconds to wait for rdumtool output..."
	sleep 5
	DOOL_LINES=`wc -l /tmp/UM25C.txt | cut -d' ' -f1`
done

if [ $VERBOSE -eq 1 ] ; then
        1>&2 echo "+ Terminaison du processus de rdumtool"
fi
kill ${RDUMTOOL_PID}

if [ $VERBOSE -eq 1 ] ; then
        1>&2 echo "+ Mise en forme du fichier de mesures de rdumtool"
fi
perl -pi -e 's|USB:\s+(.*?)V,\s+(.*?)A,\s+(.*?)W,\s+(.*?)Ω|\1,\2,\3,\4|' /tmp/UM25C.txt
perl -pi -e 's|Collection time: (\d{4}-\d{2}-\d{2}) (.*)|\1,\2|' /tmp/UM25C.txt
awk '!(NR%2){print$0","p}{p=$0}' /tmp/UM25C.txt >> /tmp/UM25C.csv

if [ $VERBOSE -eq 1 ] ; then
        1>&2 echo "+ Vérification du contenu du fichier dool.csv"
fi
DOOL_LINES=`ssh -i ${SSH_RSA_KEY} ${SSH_HOST_USER}@${SSH_HOST_IP} "wc -l dool.csv" | cut -d' ' -f1`
while [[ $DOOL_LINES -eq 0 ]]; do
    1>&2 echo "+ Sleeping 5 more seconds to wait for dool output..."
    sleep 5
    DOOL_LINES=`ssh -i ${SSH_RSA_KEY} ${SSH_HOST_USER}@${SSH_HOST_IP} "wc -l dool.csv" | cut -d' ' -f1`
done

if [ $VERBOSE -eq 1 ] ; then
        1>&2 echo "+ Terminaison du processus de dool"
fi
ssh -i ${SSH_RSA_KEY} ${SSH_HOST_USER}@${SSH_HOST_IP} "kill ${DOOL_PID}"

if [ $VERBOSE -eq 1 ] ; then
        1>&2 echo "+ Récupération du fichier dool.csv"
fi
scp -i ${SSH_RSA_KEY} ${SSH_HOST_USER}@${SSH_HOST_IP}:dool.csv ./dool.tmp.csv

if [ $VERBOSE -eq 1 ] ; then
        1>&2 echo "+ Suppression des en-têtes superflus du fichier dool.csv"
fi
tail -n +`grep -n "usr" dool.tmp.csv | cut -d':' -f1` dool.tmp.csv > dool.csv

rm -f dool.tmp.csv

cp /tmp/UM25C.csv .

if [ $VERBOSE -eq 1 ] ; then
        1>&2 echo "+ Récupération du fichier test.csv"
fi
scp -i ${SSH_RSA_KEY} ${SSH_HOST_USER}@${SSH_HOST_IP}:/tmp/test.csv .

echo "Script terminé ! Les nouveaux fichiers sont UM25C.csv, dool.csv et test.csv"
