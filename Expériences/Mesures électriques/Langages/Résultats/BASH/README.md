# Consommation énergétique pour le langage BASH

## Objectifs
* Mesure de la consommation du programme copying.bash
* Voir méthode dans le README du répertoire LANG

## Contenu du répertoire
* B.gp : fichier gnuplot d'analyse avec des marges adaptées au langage BASH
(15 secondes avant, 120 secondes après)
* Mxx : fichier d'observation issu de ''read-multimetre.bash > Mxx''
* Bxx : fichier de log issu de ''test-B.bash > Bxx''
* g-Bxx.png : graphique correspondant obtenu avec gnuplot -pc B.gp

## Résultats
* g-B40.png : 18,919 J par exécution

## Conclusion



