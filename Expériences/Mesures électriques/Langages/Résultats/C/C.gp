# Script gnuplot pour analyser les résultats des tests d'un programme
# Vocabulaire :
# - observation : ce qui est fourni dans le fichier du multimètre
# - échantillon : le début de l'observation, qualibré pour comparaison
#                 sa taille s'exprime en **secondes** et non en nombre de lignes
#                 car les mesures ne sont pas forcément régulières
# NB : on suppose que l'échantillon est suffisamment long pour inclure toutes
#      les exécutions indiquées en entête du fichier TEST

#-- Paramètres ----------------------------------------------------------------#
# NB : par hypothèse, on suppose que la lecture du multimètre démarre avant
#      et termine après le test.
MULT="M40"
TEST="C40"

# Marge en secondes après la fin de la dernière exécution dans le fichier TEST
# NB : la marge (surtout après) est importante car l'activité engendrée par le
#      programme dure plus longtemps que le temps de l'exécution avec les
#      éventuels caches (cache disque).
MARGEDEB=15
MARGEFIN=60

# Puissance consommée à vide pour comparaison
# NB : cf. read-multimetre.bash et RPI.gp, cf. résultats M14 dans RPI
WATTREF=1.364

XMAX=400

# Valeur maximale sur l'ordonée pour le tracé
# NB : valeur supérieure aux mesures permettant permettant l'affichage de la légende hors des courbes
YMAX=9


#-- Récupération de la date de référence de l'observation ---------------------#
# NB : par définition, c'est la première date, qui est la plus petite.
# NB : utile pour avoir des dates partant de 0 en abcisse.
#      première valeur (seconde ligne) de la première colonne
# NB : la première ligne du fichier contient la légende
stats MULT every ::1 using 1 name "DATEMULT" nooutput

DATEREF=DATEMULT_min


#-- Récupération de la date de fin de la dernière exécution -------------------#
# NB : permet de qualibrer la fin de l'intégrale
stats TEST every ::1 using ($1-DATEREF) name "DATETEST" nooutput
# La dernière date est DATETEST_max


#-- Calcul des dates de début et de fin de l'échantillon ----------------------#
# NB : par définition, ce sont les dates de l'échantillon -+ les marges
DATEDEB=DATETEST_min-MARGEDEB
DATEFIN=DATETEST_max+MARGEFIN

if(DATEDEB < 0) {
  print ""
  print "- observation trop courte avant l'exécution pour la marge"
  exit
}

if(DATEFIN > DATEMULT_max) {
  print ""
  print "- observation trop courte après l'exécution pour la marge"
  exit
}

# La durée de l'observation est celle de TEST + MARGEDEB + MARGEFIN
DURATION= DATEFIN - DATEDEB
#DATETEST_max + MARGEDEB + MARGEFIN



#-- Récupération du nombre d'itérations ---------------------------------------#
# NB : il est en troisième position sur la première ligne du fichier TEST
# NB : Si on considère la troisième colonne (using 3),
#      il faut relever la valeur de la première ligne,
#      premier bloc jusque première ligne, premier bloc.
stats TEST every 1:1:0:0:0:0 using 3 name "LOOP" nooutput
# Le nombre de boucles est la plus grande valeur de cette unique valeur,
# c'est-à-dire LOOP_max.

#-- Récupération de la plus grande date de l'observation ----------------------#
# NB : utile pour tracer la ligne des Watt de référence
stats MULT every ::1 using ($1-DATEREF) name "DATE" nooutput


#-- Calcul de l'intégrale des Watt de l'échantillon uniquement ----------------#
# NB : la plage de calcul commence à 0 et termine à DURATION (en secondes)

# Appliquée à l'abscisse, delta() calcule la différence
# entre deux dates.
delta(x) = ( delta = x - old_x, old_x = x, delta )
old_x = NaN

# Appliquée aux watts, mid() calcule la moyenne entre deux
# valeurs successives
mid(y) = ( mid = (y + old_y)/2, old_y = y, mid)
old_y = NaN

# L'intégrale est calculée par la somme des rectangles de l'échantillon,
# c'est-à-dire tant que la date n'a pas dépassé la durée de l'échantillon.
stats MULT every ::1 using ( ( ( ($1-DATEREF) < DATEDEB) || ( ($1-DATEREF) > DATEFIN) ) ? 0 : (delta($1)*mid($4)) ) name "CALC"
#stats MULT every ::1 using ( ((($1 - DATEREF) < DATEDEB) || (($1-DATEREF) > DATEFIN)) ? 0 : 1) name "CALC"
#nooutput

CONSO=(CALC_sum - DURATION * WATTREF) / LOOP_max
#print "Consommation par test (mWs):"
#print CONSO


#-- Affichages ----------------------------------------------------------------#
print "+ Date de référence de l'observation (première date) en secondes.nanosecondes :"
print DATEREF
print "+ Durée de l'observation en secondes :"
print DATE_max
print "+ Puissance instantanée de référence utilisée (WATTREF) en watts :"
print WATTREF

print ""

print "+ Durée de l'échantillon étudiée (partant de 0) en secondes (marges inclues) : "
print DURATION

print "+ Nombre d'exécutions : "
print LOOP_max
print "+ Consommation énergétique pour l'échantillon en joules :"
print CALC_sum
print "+ Consommation énergétique de référence pour la même durée :"
print DURATION * WATTREF

print "+ Consommation énergétique par exécution en joules :"
print CONSO

#-- Tracé ---------------------------------------------------------------------#

set xlabel "temps (secondes)"

set xrange [0:XMAX]
set yrange [0:YMAX]

set label 1 sprintf("Durée de l'échantillon avec %g exécutions (marges de %g s et %g s) : %g s", LOOP_max, MARGEDEB, MARGEFIN, DURATION) at 0, 8.75
set label 2 sprintf("Consommation de référence avec WATTREF=%g W : %g J", WATTREF, DURATION * WATTREF) at 0, 8.5
set label 3 sprintf("Intégrale des watts sur le test+marge=%g J", CALC_sum) at 0, 8
set label 4 sprintf("Consommation par exécution=%g J", CONSO) at 0, 7.75

# Tracé de WATTREF
set arrow 1 from 0,WATTREF to XMAX,WATTREF nohead 

# Tracé des bornes de l'échantillon
set arrow 2 from DATEDEB,0 to DATEDEB,7 nohead linecolor 6 linewidth 3
set arrow 3 from DATEFIN,0 to DATEFIN,7 nohead linecolor 6 linewidth 3

# every ::1 pour ignorer la première ligne (légende)
plot MULT  every ::1 using ($1-DATEREF):2 with line  linecolor 3 title columnheader(2)
replot MULT every ::1 using ($1-DATEREF):3 with line linecolor 4 title columnheader(3)
replot MULT every ::1 using ($1-DATEREF):4 with line linecolor 5 linewidth 2 title columnheader(4)

replot MULT every ::1 using ($1-DATEREF):5 with line linecolor 6 title columnheader(5)
replot MULT every ::1 using ($1-DATEREF):6 with line linecolor 7 title columnheader(6)

replot TEST every ::1 using ($1-DATEREF):2 with line linecolor 9 title columnheader(2)






# NB : stats fait un résumé statistique
#      Les données sont dans STATS_min, STATS_max...
#      sauf si name DATE : elles sont dans DATE_min, DATE_max...
# NB : every permet de sélectionner une partie des donées :
#      point increment
#      block increment
#      ligne de départ (la numérotation commence à 0)
#      bloc de départ
#      ligne de fin
#      bloc de fin
#stats MULT every ::1::1 using 1 name "FIRSTMULT" nooutput 
# La date de référence est la plus petite date de cette unique première date...
#DATEREF=FIRSTMULT_min
