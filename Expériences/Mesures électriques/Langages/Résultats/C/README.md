# Consommation énergétique pour le langage C

## Objectifs
* Mesure de la consommation du programme copying.c
* Voir méthode dans le README du répertoire LANG

## Contenu du répertoire
* C.gp : fichier gnuplot d'analyse avec des marges adaptées au langage C
(15 secondes avant, 60 secondes après)
* Mxx : fichier d'observation issu de ''read-multimetre.bash > Mxx''
* Cxx : fichier de log issu de ''test-C-remote.bash > Cxx''
* g-Cxx.png : graphique correspondant obtenu avec gnuplot -pc C.gp

## Résultats
* g-C30.png : 2,197 J par exécution
* g-C31.png : 2,034 J
* g-C32.png : 2,192 J

## Conclusion
* Résultats assez semblables d'une exécution à l'autre.
* Moyenne de 2,141 J par exécution pour ces trois essais.


