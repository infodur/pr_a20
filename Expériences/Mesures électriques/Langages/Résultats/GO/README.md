# Consommation énergétique pour le langage GO

## Objectifs
* Mesure de la consommation du programme copying.go
* Voir méthode dans le README du répertoire LANG

## Contenu du répertoire
* Mxx : fichier d'observation issu de ''read-multimetre-v2.bash''
* Gxx : fichier de log issu de ''./execute.c go 10 .old > Gxx''


## Résultats
TODO

## Conclusion
TODO

## TO DO :
* Gnuplot
* Conclusion et interprétation des résultats 