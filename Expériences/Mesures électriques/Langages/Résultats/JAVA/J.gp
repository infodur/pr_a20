# Script gnuplot pour analyser les resultats des tests d'un programme
# Vocabulaire :
# - observation : ce qui est fourni dans le fichier du multimetre
# - echantillon : le debut de l'observation, qualibre pour comparaison
#                 sa taille s'exprime en **secondes** et non en nombre de lignes
#                 car les mesures ne sont pas forcement regulieres
# NB : on suppose que l'echantillon est suffisamment long pour inclure toutes
#      les executions indiquees en entête du fichier TEST

#-- Parametres ----------------------------------------------------------------#
# NB : par hypothese, on suppose que la lecture du multimetre demarre avant
#      et termine apres le test.
MULT="M03"
TEST="J03"

# Marge en secondes apres la fin de la derniere execution dans le fichier TEST
# NB : la marge (surtout apres) est importante car l'activite engendree par le
#      programme dure plus longtemps que le temps de l'execution avec les
#      eventuels caches (cache disque).
MARGEDEB=5
MARGEFIN=60

# Puissance consommee à vide pour comparaison
# NB : cf. read-multimetre.bash et RPI.gp, cf. resultats M14 dans RPI
WATTREF=1.387

XMAX=300

# Valeur maximale sur l'ordonee pour le trace
# NB : valeur superieure aux mesures permettant permettant l'affichage de la legende hors des courbes
YMAX=9


#-- Recuperation de la date de reference de l'observation ---------------------#
# NB : par definition, c'est la premiere date, qui est la plus petite.
# NB : utile pour avoir des dates partant de 0 en abcisse.
#      premiere valeur (seconde ligne) de la premiere colonne
# NB : la premiere ligne du fichier contient la legende
stats MULT every ::1 using 1 name "DATEMULT" nooutput

DATEREF=DATEMULT_min


#-- Recuperation de la date de fin de la derniere execution -------------------#
# NB : permet de qualibrer la fin de l'integrale
stats TEST every ::1 using ($1-DATEREF) name "DATETEST" nooutput
# La derniere date est DATETEST_max


#-- Calcul des dates de debut et de fin de l'echantillon ----------------------#
# NB : par definition, ce sont les dates de l'echantillon -+ les marges
DATEDEB=DATETEST_min-MARGEDEB
DATEFIN=DATETEST_max+MARGEFIN

if(DATEDEB < 0) {
  print ""
  print "- observation trop courte avant l'execution pour la marge"
  exit
}

if(DATEFIN > DATEMULT_max) {
  print ""
  print "- observation trop courte apres l'execution pour la marge"
  exit
}

# La duree de l'observation est celle de TEST + MARGEDEB + MARGEFIN
DURATION= DATEFIN - DATEDEB
#DATETEST_max + MARGEDEB + MARGEFIN



#-- Recuperation du nombre d'iterations ---------------------------------------#
# NB : il est en troisieme position sur la premiere ligne du fichier TEST
# NB : Si on considere la troisieme colonne (using 3),
#      il faut relever la valeur de la premiere ligne,
#      premier bloc jusque premiere ligne, premier bloc.
stats TEST every 1:1:0:0:0:0 using 3 name "LOOP" nooutput
# Le nombre de boucles est la plus grande valeur de cette unique valeur,
# c'est-à-dire LOOP_max.

#-- Recuperation de la plus grande date de l'observation ----------------------#
# NB : utile pour tracer la ligne des Watt de reference
stats MULT every ::1 using ($1-DATEREF) name "DATE" nooutput


#-- Calcul de l'integrale des Watt de l'echantillon uniquement ----------------#
# NB : la plage de calcul commence à 0 et termine à DURATION (en secondes)

# Appliquee à l'abscisse, delta() calcule la difference
# entre deux dates.
delta(x) = ( delta = x - old_x, old_x = x, delta )
old_x = NaN

# Appliquee aux watts, mid() calcule la moyenne entre deux
# valeurs successives
mid(y) = ( mid = (y + old_y)/2, old_y = y, mid)
old_y = NaN

# L'integrale est calculee par la somme des rectangles de l'echantillon,
# c'est-à-dire tant que la date n'a pas depasse la duree de l'echantillon.
stats MULT every ::1 using ( ( ( ($1-DATEREF) < DATEDEB) || ( ($1-DATEREF) > DATEFIN) ) ? 0 : (delta($1)*mid($4)) ) name "CALC"
#stats MULT every ::1 using ( ((($1 - DATEREF) < DATEDEB) || (($1-DATEREF) > DATEFIN)) ? 0 : 1) name "CALC"
#nooutput

CONSO=(CALC_sum - DURATION * WATTREF) / LOOP_max
#print "Consommation par test (mWs):"
#print CONSO


#-- Affichages ----------------------------------------------------------------#
print "+ Date de reference de l'observation (premiere date) en secondes.nanosecondes :"
print DATEREF
print "+ Duree de l'observation en secondes :"
print DATE_max
print "+ Puissance instantanee de reference utilisee (WATTREF) en watts :"
print WATTREF

print ""

print "+ Duree de l'echantillon etudiee (partant de 0) en secondes (marges inclues) : "
print DURATION

print "+ Nombre d'executions : "
print LOOP_max
print "+ Consommation energetique pour l'echantillon en joules :"
print CALC_sum
print "+ Consommation energetique de reference pour la même duree :"
print DURATION * WATTREF

print "+ Consommation energetique par execution en joules :"
print CONSO

#-- Trace ---------------------------------------------------------------------#

set xlabel "temps (secondes)"

set xrange [0:XMAX]
set yrange [0:YMAX]

set label 1 sprintf("Duree de l'echantillon avec %g executions (marges de %g s et %g s) : %g s", LOOP_max, MARGEDEB, MARGEFIN, DURATION) at 0, 8.75
set label 2 sprintf("Consommation de reference avec WATTREF=%g W : %g J", WATTREF, DURATION * WATTREF) at 0, 8.5
set label 3 sprintf("Integrale des watts sur le test+marge=%g J", CALC_sum) at 0, 8
set label 4 sprintf("Consommation par execution=%g J", CONSO) at 0, 7.75

# Trace de WATTREF
set arrow 1 from 0,WATTREF to XMAX,WATTREF nohead 

# Trace des bornes de l'echantillon
set arrow 2 from DATEDEB,0 to DATEDEB,7 nohead linecolor 6 linewidth 3
set arrow 3 from DATEFIN,0 to DATEFIN,7 nohead linecolor 6 linewidth 3

# every ::1 pour ignorer la premiere ligne (legende)
plot MULT  every ::1 using ($1-DATEREF):2 with line  linecolor 3 title columnheader(2)
replot MULT every ::1 using ($1-DATEREF):3 with line linecolor 4 title columnheader(3)
replot MULT every ::1 using ($1-DATEREF):4 with line linecolor 5 linewidth 2 title columnheader(4)

replot MULT every ::1 using ($1-DATEREF):5 with line linecolor 6 title columnheader(5)
replot MULT every ::1 using ($1-DATEREF):6 with line linecolor 7 title columnheader(6)

replot TEST every ::1 using ($1-DATEREF):2 with line linecolor 9 title columnheader(2)






# NB : stats fait un resume statistique
#      Les donnees sont dans STATS_min, STATS_max...
#      sauf si name DATE : elles sont dans DATE_min, DATE_max...
# NB : every permet de selectionner une partie des donees :
#      point increment
#      block increment
#      ligne de depart (la numerotation commence à 0)
#      bloc de depart
#      ligne de fin
#      bloc de fin
#stats MULT every ::1::1 using 1 name "FIRSTMULT" nooutput 
# La date de reference est la plus petite date de cette unique premiere date...
#DATEREF=FIRSTMULT_min
