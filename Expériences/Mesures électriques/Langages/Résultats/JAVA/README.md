# Consommation énergétique pour le langage GO

## Objectifs
* Mesure de la consommation du programme copying.java
* Voir méthode dans le README du répertoire LANG

## Contenu du répertoire
* Mxx : fichier d'observation issu de ''read-multimetre-v2.bash''
* Jxx : fichier de log issu de ''./execute.c java 10 .old > Jxx''


## Résultats
TODO

## Conclusion
TODO

## TO DO :
* Gnuplot
* Conclusion et interprétation des résultats 