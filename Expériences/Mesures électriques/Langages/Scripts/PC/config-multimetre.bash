echo "$0 : connexion au multimètre UM25C"

ID=`id | cut -d '=' -f2 | cut -d '(' -f 1`


if [ $ID != 0 ]; then
		echo "- usage: sudo $0"
		exit
else
		echo "+ sudo ok"
fi
		
echo "* vérifier que l'interrupteur du multimètre est sur \"on\""
echo "+ la lumière bleue doit être clignotante"

echo "+ Désactivation préventive du bluetooth"
rfkill block bluetooth
if [ $? = 1 ]; then
		echo "- échec de la désactivation préventive du bluetooth"
		exit
fi

echo "+ Activation du bluetooth"
rfkill unblock bluetooth
if [ $? = 1 ]; then
		echo "- échec activation du bluetooth"
		exit
fi

echo "+ Recherche de l'adresse bluetooth du multimètre"

# NB : hcitool scan retourne l'adresse mais pas le nom
MAC=`bt-device -l | grep UM25C | cut -d '(' -f2 | cut -d')' -f 1`

if [ "x$MAC" = "x" ]; then
		echo "- multimètre UM25C non trouvé ; ajoutez-le à vos périphériques de confiance"
		exit
else
		echo "+ adresse MAC trouvée : $MAC"
fi
		

echo "+ Déconnexion préventive du périphérique"
bt-device -d $MAC
if [ $? = 1 ]; then
		echo "- échec de la déconnexion préventive"
		exit
fi

echo "+ Connexion au périphérique"
# bt-device -c a fonctionne mais ne fonctionne plus...
#bt-device -c $MAC
hcitool cc $MAC
if [ $? = 1 ]; then
		echo "- échec de la connexion"
		exit
fi

echo "+ Libération préventive de l'association"
rfcomm release $MAC
if [ $? = 1 ]; then
		echo "- échec libération du cannal 0 (rfcomm release)"
		exit
fi


echo "+ Association au cannal 0"
rfcomm bind $MAC 0
if [ $? = 1 ]; then
		echo "- échec association au cannal 0 (rfcomm bind)"
		exit
fi

echo "+ Ready"


		 
