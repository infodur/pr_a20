#-- Paramètres ----------------------------------------------------------------#
# Séparateur de champs dans les résultats
# NB : dépend du logiciel utilisé pour analyser les résultats
#      (espace pour gnuplot)
SEP=' '

# Séparateur de décimale selon la LOCALE
# NB : tester avec la commande date par exemple
VIRIN=','

# Séparateur de décimale souhaité en sorti
# NB : dépend du logiciel/PC utilisé pour analyser les résultats
#      (gnuplot affecté par la LOCALE ?)
VIROUT='.'

# Répertoire de rdumtool
DIR=./rdumtool


#-- Options -------------------------------------------------------------------#

# Option VERBOSE -v pour des affichages sur stderr pendant l'exécution
VERBOSE=0
if [ "x$1" = "x-v" ]; then
		VERBOSE=1
fi

# Option HELP -h
if [ "x$1" = "x-h" ]; then
		echo "+ $0 : lecture infinie du multimètre"
		echo "  usage : $0 > fichier_resultat (éditer $0 pour les paramètres)"
		echo "          option -v : affichages pendant l'exécution"
		echo "          option -h : affichaged de cette aide"
		exit
fi

# Cltr C => interruption du while
terminate() {

		perl -pi -e 's|USB:\s+(.*?)V,\s+(.*?)A,\s+(.*?)W,\s+(.*?)Ω.*\s(.*?)Ah,\s(.*?)Wh,\s(.*?)\ssec.*|\1 \2 \3 \5 \6|' data.txt
		1>&2 echo "+ fin"
		exit
}
trap "terminate" INT


#-- Affichage -----------------------------------------------------------------#
# NB : sur stderr pour ne pas polluer le fichier résultat avec $0 > res
1>&2 echo "$0 : lecture périodique du multimètre"
1>&2 echo "+ PID=$BASHPID"
1>&2 echo "+ Ctrl C => fin"

MAC=`bt-device -l | grep UM25C | cut -d '(' -f2 | cut -d')' -f 1`
1>&2 echo "+ Adresse MAC bluetooth du multimètre = $MAC"


# Écriture sur stdout du format pour la ligne d'entête du fichier résultat
# avec $0 > res
1>&2 echo "+ Écriture de la ligne d'entête des résultats (stdout)"
echo "Date $SEP Volt $SEP Ampère $SEP Watt $SEP Ampère-heure $SEP Watt-heure" > data.txt

1>&2 echo "+ Début de la boucle infinie, résultat sur stdout, interruption avec Ctrl C"


while true
do
		# Récupération des données + date
		DATA=`$DIR/rdumtool --device-type=UM25C --bluetooth-device=$MAC 2> /dev/null` 
		TIME=`date +%s,%N`
		echo $TIME $DATA >> data.txt
done
