#-- Paramètres ----------------------------------------------------------------#
# Séparateur de champs dans les résultats
# NB : dépend du logiciel utilisé pour analyser les résultats
#      (espace pour gnuplot)
SEP=' '

# Séparateur de décimale selon la LOCALE
# NB : tester avec la commande date par exemple
VIRIN=','

# Séparateur de décimale souhaité en sorti
# NB : dépend du logiciel/PC utilisé pour analyser les résultats
#      (gnuplot affecté par la LOCALE ?)
VIROUT='.'

# Répertoire de rdumtool
DIR=./rdumtool-master


#-- Options -------------------------------------------------------------------#

# Option VERBOSE -v pour des affichages sur stderr pendant l'exécution
VERBOSE=0
if [ "x$1" = "x-v" ]; then
		VERBOSE=1
fi

# Option HELP -h
if [ "x$1" = "x-h" ]; then
		echo "+ $0 : lecture infinie du multimètre"
		echo "  usage : $0 > fichier_resultat (éditer $0 pour les paramètres)"
		echo "          option -v : affichages pendant l'exécution"
		echo "          option -h : affichaged de cette aide"
		exit
fi

# Cltr C => interruption du while
terminate() {
		1>&2 echo "+ fin"
		exit
}
trap "terminate" INT


#-- Affichage -----------------------------------------------------------------#
# NB : sur stderr pour ne pas polluer le fichier résultat avec $0 > res
1>&2 echo "$0 : lecture périodique du multimètre"
1>&2 echo "+ PID=$BASHPID"
1>&2 echo "+ Ctrl C => fin"

MAC=`bt-device -l | grep UM25C | cut -d '(' -f2 | cut -d')' -f 1`
1>&2 echo "+ Adresse MAC bluetooth du multimètre = $MAC"


# Écriture sur stdout du format pour la ligne d'entête du fichier résultat
# avec $0 > res
1>&2 echo "+ Écriture de la ligne d'entête des résultats (stdout)"
echo "Date $SEP Volt $SEP Ampère $SEP Watt $SEP Ampère-heure $SEP Watt-heure $SEP Données non formatées pour vérification"


1>&2 echo "+ Début de la boucle infinie, résultat sur stdout, interruption avec Ctrl C"

while true
do
		# Récupération des données + date
		DATA=`$DIR/rdumtool --device-type=UM25C --bluetooth-device=$MAC 2> /dev/null`
		TIME=`date +%s,%N`
		
		# Extraction des données
		# NB : il se pourrait que les champs changent selon le mode de
		#      rdumtool, d'où les vérifications ci-dessous avec les unités
		V=`echo $DATA | tr -s ' ' | cut -d ' ' -f 2`
		A=`echo $DATA | tr -s ' ' | cut -d ' ' -f 3`
		W=`echo $DATA | tr -s ' ' | cut -d ' ' -f 4`
		Ah=`echo $DATA | tr -s ' ' | cut -d ' ' -f 16`
		Wh=`echo $DATA | tr -s ' ' | cut -d ' ' -f 17`

		# Formatage + vérification de la présence de l'unité
		V2=${V/V,/}
		if [ "x$V2" = "x$V" ]; then
				echo "- Champ volt erroné : $V"
				exit
		fi
		V2=${V2/$VIRIN/$VIROUT}

		A2=${A/A,/}
		if [ "x$A2" = "x$A" ]; then
				echo "- Champ ampère erroné : $A"
				exit
		fi
		A2=${A2/$VIRIN/$VIROUT}

		W2=${W/W,/}
		if [ "x$W2" = "x$W" ]; then
				echo "- Champ watt erroné : $W"
				exit
		fi
		W2=${W2/$VIRIN/$VIROUT}

		Ah2=${Ah/Ah,/}
		if [ "x$Ah2" = "x$Ah" ]; then
				echo "- Champ ampère-heure erroné : $Ah"
				exit
		fi
		Ah2=${Ah2/$VIRIN/$VIROUT}

		Wh2=${Wh/Wh,/}
		if [ "x$Wh2" = "x$Wh" ]; then
				echo "- Champ watt-heure erroné : $Wh"
				exit
		fi
		Wh2=${Wh2/$VIRIN/$VIROUT}

		# Affichage Volt Ampère Watt Ampère-heure Watt-heure
		# NB : les deux dernières mesures sont cumulatives si le multimètre
		#      est en mode enregistrement.
		# NB : ajout des valeurs non formatées avec leurs unités pour
		#      post-vérification.
		echo ${TIME/$VIRIN/$VIROUT} $SEP $V2 $SEP $A2 $SEP $W2 $SEP $Ah2 $SEP $Wh2 $SEP \"$V $A $W $Ah $Wh\"
 
		if [ $VERBOSE -eq 1 ] ; then
				1>&2 echo ${TIME/$VIRIN/$VIROUT} $SEP $V2 $SEP $A2 $SEP $W2 $SEP $Ah2 $SEP $Wh2 $SEP \"$V $A $W $Ah $Wh\"
		fi

done
