# Ce script exécute sur une cible (RPI) un programme via un script qui retourne
# la date et le résultat du programme.

#-- Paramètres ----------------------------------------------------------------#
#-- Format des résultats --------#
# Séparateur de champs dans les résultats
# NB : dépend du logiciel utilisé pour analyser les résultats
#      (espace pour gnuplot)
SEP=' '

# Séparateur de décimale selon la LOCALE
# NB : tester avec la commande date par exemple
VIRIN='.'

# Séparateur de décimale souhaité en sorti
# NB : dépend du logiciel/PC utilisé pour analyser les résultats
#      (gnuplot affecté par la LOCALE ?)
VIROUT='.'

#-- Identification de la cible --#
# Adresse IP de la RPI
RPI_IP=192.168.100.25

# User sur la RPI
RPI_USER=infodur

# Identité SSH (pour éviter de rentrer le mot de passe en ssh)
SSH_ID=id_rsa_rpi

# Emplacement sur la RPI du script de lancement avec mesure du temps
RPI_START=/home/infodur/BIN/start.bash


#-- Programme à exécuter --------#
# Exécutable
RPI_PROG="/home/$RPI_USER/BIN/copying.bash"

# Répertoire de base contenant les répertoires de tests
RPI_BASE="/home/$RPI_USER/TESTDIR"

# Préfixe des noms de répertoires de tests
RPI_DIR="TESTDIR-"

# Extension pour copying
RPI_EXT=".old"

# Nom du test
NAME_TEST=`basename --suffix=.bash $0`

# Nombre d'itérations
BEGDIR=1
# Nombre d'itérations
ENDDIR=10

# Pause entre deux essais (en secondes)
PAUSE=5




#-- Options -------------------------------------------------------------------#

# Option VERBOSE -v pour des affichages sur stderr pendant l'exécution
VERBOSE=0
if [ "x$1" = "x-v" ]; then
		VERBOSE=1
fi

# Option HELP -h
if [ "x$1" = "x-h" ]; then
		echo "+ $0 : tests à distance du programme C"
		echo "  usage : $0 > fichier_resultat (éditer $0 pour les paramètres)"
		echo "          option -v : affichages pendant l'exécution"
		echo "          option -h : affichage de cette aide"
		exit
fi

# Cltr-C => interruption du while
terminate() {
		1>&2 echo "+ fin"
		exit
}
trap "terminate" INT


#-- Affichages ----------------------------------------------------------------#
# NB : écriture sur stderr pour pouvoir récupérer le fichier de mesures
#      avec $0 > mesures
1>&2 echo "$0 : lancement des tests en C"
1>&2 echo "+ Cible :"
1>&2 echo "+   Utilisateur :  $RPI_USER"
1>&2 echo "+   Raspberry :    $RPI_IP"
1>&2 echo "+   Script local : $RPI_START"

1>&2 echo "+ Programme de tests"
1>&2 echo "+   Exécutable : $RPI_PRG"
1>&2 echo "+   Répertoire : $RPI_DIR"
1>&2 echo "+   Extension :  $RPI_EXT"

1>&2 echo "+ Contrôle des tests :"
1>&2 echo "+   Pause (en secondes) : $PAUSE"
1>&2 echo "+   PID de ce script : $BASHPID"
1>&2 echo "+   Terminaison avec Ctrl-C"


#-- Préparatifs ---------------------------------------------------------------#
# Extrait de man date :
#   %s     seconds since 1970-01-01 00:00:00 UTC
#   %N     nanoseconds (000000000..999999999)
DATE_LO1=`date +%s.%N`
DATE_RPI=`ssh $RPI_USER@$RPI_IP -i $SSH_ID "date +%s.%N"`
DATE_LO2=`date +%s.%N`

1>&2 echo "+ Vérification des dates"
1>&2 echo "+   Format des dates : nb secondes depuis 1970, nanosecondes"
1>&2 echo "+   locale=$DATE_LO1, RPI=$DATE_RPI, locale=$DATE_LO2"

# Long ! Vérifier avec test-NTP.bash plutôt.
#1>&2 echo "+  offset ntp"
#1>&2 ntpdate -q $RPI_IP

# Nombre d'itérations
N=$((ENDDIR-BEGDIR+1))

# Écriture sur stdout du format pour la ligne d'entête du fichier résultat
# avec $0 > res
1>&2 echo "+ Écriture de la ligne d'entête des résultats (stdout)"
echo "Date $SEP $NAME_TEST $SEP $N $SEP \"dates locale/RPI/locale :\" $SEP $DATE_LO1 $SEP $DATE_RPI $SEP $DATE_LO2"


#-- Boucle --------------------------------------------------------------------#
1>&2 echo "+ Début de la boucle (répertoires $BEGDIR à $ENDDIR, $N iterations),
résultat sur stdout, interruption avec Ctrl-C"

for I in `seq $BEGDIR $ENDDIR` ; do
		if [ $VERBOSE -eq 1 ] ; then
				1>&2 echo "+   Pause $PAUSE secondes"
		fi
		sleep $PAUSE

		if [ $VERBOSE -eq 1 ] ; then
				1>&2 echo "+   Exécution de : ssh $RPI_USER@$RPI_IP -i $SSH_ID $RPI_START \"$RPI_PROG $RPI_BASE/${RPI_DIR}$I $RPI_EXT\" "
		fi
		ssh $RPI_USER@$RPI_IP -i $SSH_ID $RPI_START "$RPI_PROG $RPI_BASE/${RPI_DIR}$I $RPI_EXT"

done
