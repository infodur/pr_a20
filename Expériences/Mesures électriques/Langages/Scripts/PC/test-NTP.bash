#-- Paramètres ----------------------------------------------------------------#
# Séparateur de champs dans les résultats
# NB : dépend du logiciel utilisé pour analyser les résultats
#      (espace pour gnuplot)
SEP=' '

# Séparateur de décimale selon la LOCALE
# NB : tester avec la commande date par exemple
VIRIN='.'

# Séparateur de décimale souhaité en sorti
# NB : dépend du logiciel/PC utilisé pour analyser les résultats
#      (gnuplot affecté par la LOCALE ?)
VIROUT='.'

# Adresse IP de la RPI
RPI_IP=192.168.100.25

# Nombre de tests
N=1000


#-- Options -------------------------------------------------------------------#

# Option VERBOSE -v pour des affichages sur stderr pendant l'exécution
VERBOSE=0
if [ "x$1" = "x-v" ]; then
		VERBOSE=1
fi

# Option HELP -h
if [ "x$1" = "x-h" ]; then
		echo "+ $0 : mesure de l'offset avec une machine du réseau local"
		echo "  usage : $0 > fichier_resultat (éditer $0 pour les paramètres)"
		echo "          option -v : affichages pendant l'exécution"
		echo "          option -h : affichage de cette aide"
		exit
fi

# Cltr C => interruption du while
terminate() {
		1>&2 echo "+ fin"
		exit
}
trap "terminate" INT


#-- Affichage -----------------------------------------------------------------#
# NB : sur stderr pour ne pas polluer le fichier résultat avec $0 > res
1>&2 echo "+ $0 : $N tests NTP de $RPI_IP (interruption par Ctrl-C)..."


# Ecriture du format sur stdout
# NB : stdout -> récupération dans le fichier résultat avec $0 > res
#                et exploitation par le script gnuplot pour la légende
1>&2 echo "+ Écriture de la ligne d'entête des résultats (stdout)"
echo "secondes${VIROUT}nanosecondes $SEP \"offset (millisecondes)\""

1>&2 echo "+ Début de la boucle, résultats sur stdout, interruption avec Ctrl C"
for I in `seq $N` ; do
		if [ $VERBOSE -eq 1 ] ; then
				1>&2 echo "+ $0 :   requête n°$I"
		fi
		OFFSET=`ntpdate -q $RPI_IP | head -1 | cut -d ' ' -f 6 | cut -d ',' -f 1`
		DATE=`date +%s.%N`

		# Conversion de l'offset en millisecondes
		OFFSETms=`echo "$OFFSET * 1000" | bc`

		# Affichage sur stdout et stderr
		# stdout : pour récupération dans un fichier via $0 > res
		# stderr : pour suivre l'exécution en temps réel
		echo ${DATE/$VIRIN/$VIROUT} $SEP ${OFFSETms/$VIRIN/$VIROUT}
		if [ $VERBOSE -eq 1 ] ; then
				1>&2 echo ${DATE/$VIRIN/$VIROUT} $SEP ${OFFSETms/$VIRIN/$VIROUT}
		fi
done

1>&2 echo "+ $0 : fin des $N requêtes"
