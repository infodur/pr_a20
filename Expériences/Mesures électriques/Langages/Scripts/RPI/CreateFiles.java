import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class CreateFiles {
    public static void main(String[] args) throws IOException {
        String path = args[0];
        int nbFiles = Integer.parseInt(args[1]);

        for (int i = 1; i <= nbFiles; i++){
            //File f = new File(path+"\\fichier"+i+".txt");
            //f.createNewFile();

            FileWriter f = new FileWriter(path+"/fichier"+i+".txt");
            f.write("je suis le fichier "+i);
            f.flush();
            f.close();
        }
    }
}
