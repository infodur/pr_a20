#!/bin/bash

EXE=`basename $0`

if ! [ $# -eq 2 ] || [ "x$1" = "x-h" ] || [ "x$1" = "x--help" ]; then
		echo "+ $EXE :"
		echo "    copy all files of a directory into new ones with a suffix\n"
		echo "    usage:   $EXE directory suffix"
		echo "    example: $EXE src \".old\""
		exit
fi

DIR=$1
SUF=$2

if ! [ -d $DIR ]; then
		echo "- $EXE : directory $DIR not found"
		exit
fi


for F in `ls $DIR` ; do
		if [ -f $DIR/$F ]; then
				\cp -f $DIR/$F $DIR/${F}$SUF
		fi
done
