#include <stdio.h>     /* printf, sprintf */
#include <stdlib.h>    /* EXIT_FAILURE, EXIT_SUCCESS, mkdtemp */
#include <string.h>    /* strcmp */
#include <libgen.h>    /* basename */
#include <sys/types.h> /* opendir, open */
#include <dirent.h>    /* opendir, readir */
#include <sys/stat.h>  /* open */
#include <fcntl.h>     /* open */
#include <unistd.h>    /* read */

#define BUFSIZE 8192
#define MSGSIZE 256
	
int main(int argc, char **argv)
{
	char *exe=NULL;       /* name of the program for display purpose */
	char newname[256];    /* new file name */
	char oldname[256];    /* existing file name with the directory as prefix */
	char sufname[256];    /* suffix */
	char dirnameori[256]; /* original directory name */
	char dirnametmp[256]; /* temporary directory name */
	char *s_dirnametmp;   /* temporary directory name */
	
	char message[MSGSIZE];
	char buffer[BUFSIZE];

	int d_old, d_new;
	int nr, nw, nr_all, nw_all; /* number of characters read, write and sums */
	
	DIR *p_dirori, *p_dirtmp;
	struct dirent *p_entry;
	// From the man:
	// struct dirent {
	//	 ino_t          d_ino;       /* inode number */
	//   off_t          d_off;       /* not an offset; see NOTES */
	//   unsigned short d_reclen;    /* length of this record */
	//   unsigned char  d_type;      /* type of file; not supported
	//                                  by all filesystem types */
	//   char           d_name[256]; /* filename */
	// };
	

	/* Name of th program */
	exe = basename(argv[0]);

	/* Two arguments are required except if the first is -h or --help */
	if( (argc != 3) || (strcmp(argv[1], "-h") == 0) ||
			(strcmp(argv[1], "--help") == 0) ) 
		{
			printf("+ %s :\n    copy all files of a directory into new ones named with a suffix\n", exe);
			printf("    usage:   %s directory suffix\n", exe);
			printf("    example: %s src \".old\"\n", exe);
			exit(EXIT_SUCCESS);
		}


	/* ok we suppose nom that argv[1] is a directory name and argv[2] a suffix.
     NB: recopy with strncpy useful for controling too large entries
		 (eg. malicious entries).
	*/
	strncpy(dirnameori, argv[1], 256);
	strncpy(sufname, argv[2], 256);

	
	/* Attempting to open the original directory */
	if( (p_dirori = opendir(dirnameori)) == NULL ) 
		{
			fprintf(stderr, "- %s : failed opening directory %s\n", exe, dirnameori);
			(void)snprintf(message, MSGSIZE, "- %s ", exe);
			perror(message);
			exit(EXIT_FAILURE);
		
		}

	/* ok the directory exists.
     Attemting to create a temporary directory to store the new files.
     NB: this is required because the scanning of the directory may return
         the new files among the originals, especially when there are many
				 entries and/or entries with large names. */
	(void)snprintf(dirnametmp, 256, "%s/%s", dirnameori, "tmpdir.XXXXXX");
	if( (s_dirnametmp=mkdtemp(dirnametmp)) == NULL ) 
		{
			fprintf(stderr, "- %s : failed creating temporary directory\n", exe);
			(void)snprintf(message, MSGSIZE, "- %s ", exe);
			perror(message);
			exit(EXIT_FAILURE);
		}
	

	/* Scanning the directory; for each regular entry, copying into the tmpdir */
	while( (p_entry = readdir(p_dirori)) != NULL  )
		{
			if ( p_entry->d_type == (unsigned char)DT_REG )
				{
					/* Complete name for this regular file */
					(void)snprintf(oldname, 256, "%s/%s", dirnameori, p_entry->d_name);
					/* New name with the suffix added for this file */
					(void)snprintf(newname, 256, "%s/%s%s", s_dirnametmp, p_entry->d_name, argv[2]);

					/* Attempting to open the file */
					if( (d_old = open(oldname, O_RDONLY)) < 0 )
						{
							(void)snprintf(message, MSGSIZE, "- %s : skipping %s due to opening failure", exe, oldname);
							
							perror(message);
							continue;
						}

					if( (d_new = open(newname, O_CREAT|O_WRONLY, S_IRUSR|S_IWUSR)) < 0 )
						{
							(void)snprintf(message, MSGSIZE, "- %s : skipping %s due to opening failure of new file %s", exe, oldname, newname);								
							perror(message);
							continue;
						}

					nr_all = 0;
					nw_all = 0;
					
					while( (nr = (int)read(d_old, buffer, BUFSIZE-1 )) > 0 )
						{
							nr_all += nr;
							buffer[BUFSIZE]='\0';
							
							if ( (nw = (int)write(d_new, buffer, (size_t)nr)) != nr )
								{
									(void)snprintf(message, MSGSIZE-1, "- %s : reading %d bytes in file %s but writting %d in file %s", exe, nr, oldname, nw, newname);
									perror(message);
									continue;
								}
							nw_all += nw;
						}

					if(nr_all != nw_all)
						{
							fprintf(stderr,"- %s : %d bytes read in %s but %d written in %s\n", exe, nr_all, oldname, nw_all, newname);
						}
					(void)close(d_old);
					(void)close(d_new);
				}
		}


	/* Attempting to open the temporary directory */
	if( (p_dirtmp = opendir(dirnametmp)) == NULL ) 
		{
			fprintf(stderr, "- %s : failed opening directory %s\n", exe, dirnametmp);
			(void)snprintf(message, MSGSIZE, "- %s ", exe);
			perror(message);
			exit(EXIT_FAILURE);
			
		}

	/* Scanning the temporary directory; for each regular entry, moving (renaming)
		 into the original directory */
	while( (p_entry = readdir(p_dirtmp)) != NULL  )
		{
			if ( p_entry->d_type == (unsigned char)DT_REG )
				{
					/* Complete name for this regular file */
					(void)snprintf(oldname, 256, "%s/%s", dirnametmp, p_entry->d_name);
					/* New name with the suffix added for this file */
					(void)snprintf(newname, 256, "%s/%s", dirnameori, p_entry->d_name);

					/* Attempting to rename the file to move it from the temporary
						 directory to the original one */
					if( rename(oldname, newname) != 0 )
						{
							fprintf(stderr, "- %s : failed renaming %s into %s\n", exe, oldname, newname);
							(void)snprintf(message, MSGSIZE, "- %s ", exe);
							perror(message);
							continue;
						}
				}
		}

	/* Attempting to remove the temporary directory */
	if( remove(dirnametmp) != 0 ) 
		{
			fprintf(stderr, "- %s : failed removing temporary directory %s\n", exe, dirnametmp);
			(void)snprintf(message, MSGSIZE, "- %s ", exe);
			perror(message);
			exit(EXIT_FAILURE);
		}
	
	
	exit(EXIT_SUCCESS);
}

