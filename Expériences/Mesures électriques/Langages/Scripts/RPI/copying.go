package main 

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"time"
)

func main() {

	start := time.Now()

	EXE := os.Args[0]

	if len(os.Args) != 3 || os.Args[1] == "-h" || os.Args[1] == "--help" {
		fmt.Println(EXE +
			"\ncopy all files of a directory into new ones with a suffix\n" +
			"usage : " + EXE + " directory suffix\n" +
			"example : " + EXE + "src \".old\"")
		return
	}

	folder := os.Args[1]
	ext := os.Args[2]

	// https://flaviocopes.com/go-list-files/
	//Walk prend le dir en paramètre + une WalkFunct qui sera exécutée à chaque itération.
	//type WalkFunc func(path string, info os.FileInfo, err error) error

	filepath.Walk(folder, func(path string, info os.FileInfo, err error) error {
		//path = fichier en cours

		new := path + ext //création chemin du nouveau fichier

		source, _ := os.Open(path)
		destination, _ := os.Create(new)

		_, error := io.Copy(destination, source)

		source.Close()
		destination.Close()

		if error != nil { //vérif
			fmt.Printf("The copy operation of %s failed %q\n", path, error)
		}
		return nil
	})

	end := time.Since(start)

	fmt.Printf("Temps d'exécution : %s", end)

}
