import java.io.*;
import java.util.Date;

public class copying {

    public static void main(String[] args) throws IOException {
        String EXE = "CopyingFiles"; //TODO récup dynamique du nom

        long start = System.currentTimeMillis();

        //Vérification des arguments : s'il n'y en a pas 2 ou si le premier est -h ou --help, alors on affiche l'aide
        if(args.length!=2 || args[0].equals("-h") || args[0].equals("--help")){
            System.out.println("copy all files of a directory into new ones named with a suffix" +
                    "\nusage : "+EXE+" directory suffix" +
                    "\nexemple : "+EXE+" src \".old\"");
            System.exit(0);
        }

        String path = args[0];
        String ext = args[1];


        InputStream inStream = null;
        OutputStream outStream = null;
        //Vérification du chemin vers le dossier
        File folder = new File(path);
        if(!folder.isDirectory()){
            System.out.println("Directory "+folder+" not found");
            System.exit(0);
        }

        File[] files = folder.listFiles();

        for (int i = 0; i < files.length; i++){
            if (files[i].isFile()){

                //Creates 2 files : original and copy
                File original = new File(path + "/" + files[i].getName());
                File copy = new File(path + "/" + files[i].getName()+ext);


                inStream = new FileInputStream(original);
                outStream = new FileOutputStream(copy);

                byte[]buffer = new byte[8096];

                int length;
                //copy the file content in bytes
                while ((length = inStream.read(buffer)) > 0){
                    outStream.write(buffer, 0, length);
                }

                inStream.close();
                outStream.close();
            }
        }

        long end = System.currentTimeMillis();

        System.out.println("Durée d'exécution : "+(end-start)+"ms");
    }
}


