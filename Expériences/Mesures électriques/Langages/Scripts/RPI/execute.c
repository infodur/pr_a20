#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <sys/stat.h>
#include <sys/types.h>

int main(int argc, char *argv[]){

//args = lang + nb files + nb test + ext
	char *exe =NULL;
	char ext[256];
	char lang[8];
	int nbExec;
	char nbFiles[8];
	int check;
	char commandLine[256];
	char directory[256];

	exe = basename(argv[0]);

	if( (argc != 5) || (strcmp(argv[1], "-h") == 0) || (strcmp(argv[1], "--help") == 0) ) 
	{
		printf("+ %s :\n    Executes the copying program in the given directory with the given number of files, executions and extension.\n", exe);
		printf("    usage:   %s langage nbFiles nbExec directory suffix\n", exe);
		printf("    example: %s go 1500 10 src \".old\"\n", exe);
		exit(EXIT_SUCCESS);
	}

	strncpy(lang,argv[1],8);
	strncpy(nbFiles,argv[2],8);
	nbExec = atoi(argv[3]);
	strncpy(ext,argv[4],256);

	printf("Test %s avec %d fichiers et %d exécutions",lang,nbFiles,nbExec); 


	//Boucle pour créer nbExec dossiers
	for(int i=0;i<nbExec;i++){
		strcpy(directory,"testFiles");
		strcat(directory,atoi(i));
		
		//create folder
		check = mkdir(directory); //0777
		if(!check){
			printf("Directory created %d",i);
		}
		else {
			printf("Unable to create directory %d",i);
			break;
		}

		//création dossier ok
		//créer fichiers
		strcpy(commandLine,"java ./CreateFiles testFiles/ "); // à tester, pas sûre du chemin
		strcat(commandLine,nbFiles);
		system(commandLine);//appel pg java dédié
	}

	//
	for(int i=0;i<nbExec;i++){
		/*
		strcpy(commandLine,"");
		//create folder
		check = mkdir("testFiles",0777);
		if(!check){
			printf("Directory created %d",i);
		}
		else {
			printf("Unable to create directory %d",i);
			break;
		}

		//créer fichiers
		strcpy(commandLine,"java CreateFiles testFiles/ ");
		strcat(commandLine,nbFiles);
		system(commandLine);//appel pg java dédié
		*/

		//exécuter programme
		if(strcmp(lang,"java")==0){
			strcpy(commandLine,"java copying testFiles/ ");
			strcat(commandLine,ext);
			system(commandLine); 
		}		
		else if(strcmp(lang,"go")==0){
			strcpy(commandLine,"go run copying.go testFiles/ ");
			strcat(commandLine, ext);
			system(commandLine);			
		}
		else if(strcmp(lang,"bash")==0){

		}
		else if(strcmp(lang,"c")==0){

		}
		else {
			printf("Language error");
			exit(EXIT_FAILURE);
		}

		/*
		//supprimer les fichiers
		system("rm -r testFiles/"); 
		//printf("Suppression des fichiers");
		*/
	}

	for(int i=0;i<nbExec;i++){
		strcpy(directory,"testFiles");
		strcat(directory,atoi(i));

		strcpy(commandLine,"rm -r ");
		strcat(commandLine,directory);
		strcat(commandLine,"/");
		system(commandLine); 

	}

	return 0;
}
