## Installation de Go
```wget https://dl.google.com/go/go1.15.6.linux-armv6l.tar.gz```

```sudo tar -C /usr/local -xzf go1.15.6.linux-armv6l.tar.gz```

```rm go1.15.6.linux-armv6l.tar.gz```

```nano ~/.profile```

Ajouter les 2 lignes suivantes en bas du fichier : 
```PATH=$PATH:/usr/local/go/bin```

```GOPATH=$HOME/go```

```source ~/.profile```

## Installation de Java 

```sudo apt update```

```sudo apt install default-jdk```

