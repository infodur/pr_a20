#! /usr/bin/python3
import selenium.webdriver as webdriver
import time
import os
import sys

page_url = sys.argv[1]
nb_requests = sys.argv[2]
sleep_timeout = sys.argv[3]

#options = webdriver.FirefoxOptions()
#options.add_argument('--disk-cache-size=0')
#driver = webdriver.Firefox(options=options)

#options = webdriver.ChromeOptions()
#options.add_argument('--disk-cache-size=0')
#options.add_argument('--headless')
#driver = webdriver.Chrome(options=options)


#def init():
#    driver.get('https://lemonde.fr')
#    driver.maximize_window()
#    python_button = driver.find_elements_by_xpath("/html/body/div[4]/div/div/div/div[2]/div[2]/button")[0]
#    python_button.click()

#def get_page(n,timeout):
#    for i in range(n):
#        driver.get('https://lemonde.fr')
#        print('ok')
#        time.sleep(timeout)
        
#def get_choosen_page(n,timeout,page):
#    for i in range(n):
#        driver.get(page)
#        print('ok : ' + page)
#        time.sleep(timeout)

def lynx_choosen_page(n,timeout,page):
	for i in range(n):
		os.system('lynx -cache=0 -noprint -dump   ' + page)
		print ('ok : ' + page)
		time.sleep(timeout)


#def get_page_infinite(timeout):
#    while True:
#        driver.get('https://lemonde.fr')
#        print('ok')
#        time.sleep(timeout)

lynx_choosen_page(nb_requests, sleep_timeout, page_url)
time.sleep(sleep_timeout)
print('done')
    
