#! /usr/bin/python3
import selenium.webdriver as webdriver
import time
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("url", help="page URL to target")
parser.add_argument("nbreq", type=int, help="number of requests to execute")
parser.add_argument("-t","--timeout", type=int, help="sleep timeout between each request (in sec), default is 1", default=1)
parser.add_argument("-b","--browser", help="web browser in use ('chromium' or 'firefox'), default is 'chromium'", default='chromium')
parser.add_argument("-f","--file", help="file name for CSV output", default='/tmp/test.csv')
parser.add_argument("-v", "--verbose", help="increase output verbosity",action="store_true")
args = parser.parse_args()

if args.browser == 'chromium':
    options = webdriver.ChromeOptions()
    options.add_argument('--disk-cache-size=0')
    options.add_argument('--headless')
    #Utiliser l'extension adblockplus sur chromium
    #options.add_argument('adblockpluschrome.crx')
    options.add_argument('--disable-dev-shm-usage')
elif args.browser == 'firefox':
    options = webdriver.FirefoxOptions()
    options.add_argument('--disk-cache-size=0')
    options.add_argument('--headless')

def get_page(n,timeout):
    for i in range(n):
        if args.verbose:
            print('Début requête '+str(i+1)+'/'+str(n))
        start = time.time()
        if args.browser == 'firefox':
            driver = webdriver.Firefox(options=options)
        elif args.browser == 'chromium':
            driver = webdriver.Chrome(options=options)
        driver.get(args.url)
        end = time.time()
        f.write("\"%f\",\"%f\",\"%s\"\n" % (start,end,args.url))
        if args.verbose:
            print('Requête '+str(i+1)+'/'+str(n)+' terminée, '+str(round(end-start,3))+'s nécessaires')
        start = time.time()
        driver.close()
        end = time.time()
        if args.verbose:
            print('Session web quittée, '+str(round(end-start,3))+'s nécessaires')
            print('Pause de '+str(timeout)+'s...')
        time.sleep(timeout)

f = open(args.file, "a")
if args.verbose:
    print('Début du test pour l\'URL : '+args.url)
get_page(args.nbreq,args.timeout)
f.close()
if args.verbose:
    print('Fichier CSV généré : '+args.file)
    print('Fin du programme')
