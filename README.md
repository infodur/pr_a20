# PR_A20

Vous trouverez ici les résultats de nos travaux sur les architectures logicielles économes en énergie effectués dans le cadre d'un projet d'informatique durable à l'UTC (Université de Technologie de Compiègne).

Enseignant encadrant : 
- Bertrand Ducourthial

Elèves en charge du projet: 
- Camille Beaudou
- Léa Benoit
- Nazim Bouzellat
- Pierre La Rocca
- Thomas Picouet
- Adrien Thuau
